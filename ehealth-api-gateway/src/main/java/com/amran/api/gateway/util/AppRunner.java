package com.amran.api.gateway.util;

import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.amran.api.gateway.constraint.UserStatus;
import com.amran.api.gateway.model.PersonAddressEntity;
import com.amran.api.gateway.model.PersonProfileEntity;
import com.amran.api.gateway.model.UserEntity;
import com.amran.api.gateway.model.UserRoleEntity;
import com.amran.api.gateway.repository.PersonAddressRepository;
import com.amran.api.gateway.repository.PersonProfileRepository;
import com.amran.api.gateway.repository.RoleRepository;
import com.amran.api.gateway.repository.UserRepository;

/**
 * @Author : Amran Hosssain on 6/26/2020
 */
@Component
public class AppRunner implements CommandLineRunner {

    @Autowired private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private PersonAddressRepository personAddressRepository;
    
    @Autowired
    private PersonProfileRepository personProfileRepository;
    
    @Override
    public void run(String... args) throws Exception {
        //Initially All will be delete when server startup
//        roleRepository.deleteAll();
//        roleRepository.save(new UserRoleEntity(null,"ROLE_ADMIN", UserStatus.ACTIVE.getStatus(), LocalDateTime.now()));
//        roleRepository.save(new UserRoleEntity(null,"ROLE_USER", UserStatus.ACTIVE.getStatus(), LocalDateTime.now()));
//        roleRepository.save(new UserRoleEntity(null,"ROLE_PATIENT", UserStatus.ACTIVE.getStatus(), LocalDateTime.now()));
        System.out.println("Role insert successfully.");
        
        UserEntity ue = new UserEntity();
        ue.setCreateDate(LocalDateTime.now());
        ue.setPassword(passwordEncoder.encode("password"));
        ue.setUserName("user");
        ue.setStatus(UserStatus.ACTIVE.getStatus());
//        userRepository.save(ue);
        
        PersonAddressEntity personAddressEntity = new PersonAddressEntity();
        personAddressEntity.setProvince("Jawa Barat");
        personAddressEntity.setCity("Bekasi");
        personAddressEntity.setPostalCode("412576");
        personAddressEntity.setAddressLineTwo("Perumahan Villa Indah Bekasi 1");
        personAddressEntity.setAddressLineOne("Mangunjaya, Tambun Selatan, Kab.Bekasi");
        personAddressEntity.setAddressType("HOME");
//        personAddressRepository.save(personAddressEntity);
        
        PersonProfileEntity personProfileEntity = new PersonProfileEntity();
        personProfileEntity.setCellPhone("08123456789");
        personProfileEntity.setCreateDate(LocalDateTime.now());
        personProfileEntity.setDateOfBirth(new Date());
        personProfileEntity.setEmail("cisvapery@gmail.com");
        personProfileEntity.setEmployer("Peri Purnama");
        personProfileEntity.setFirstName("Peri");
        personProfileEntity.setGender("MALE");
        personProfileEntity.setHomePhone("08123456789");
        personProfileEntity.setLastName("Purnama");
        personProfileEntity.setOccupation("OK");
//        personProfileRepository.save(personProfileEntity);
    }
}
